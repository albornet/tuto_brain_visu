# Imported python transfer function that transforms the camera output into a DC current source fed to LGN neurons
import sensor_msgs.msg
@nrp.MapRobotSubscriber("camera", Topic("/icub_model/left_eye_camera/image_raw", sensor_msgs.msg.Image))
@nrp.MapSpikeSource("LGN", nrp.map_neurons(range(0, nrp.config.brain_root.nPols*nrp.config.brain_root.imRows*nrp.config.brain_root.imCols), lambda i: nrp.brain.LGN[i]), nrp.dc_source)
@nrp.Robot2Neuron()
# def grab_image(t, camera, LGNBrightInput, LGNDarkInput):
def grab_image(t, camera, LGN):

    # Take the image from the robot's left eye
    if camera.value is not None:

        # Parameters and imports
        import numpy
        nRows     = nrp.config.brain_root.imRows
        nCols     = nrp.config.brain_root.imCols
        LGNBGain  = 1.0
        LGNDGain  = 1.0
        greyValue = 80.0  # between 0.0 and 254.0 ; neutral is 127.0

        # Read the image into an array, mean over 3 colors, resize it for the network and flatten the result
        img       = numpy.mean(CvBridge().imgmsg_to_cv2(camera.value, 'rgb8'), axis=2)

        # Crop the image (centered patch, using the Laminart network's dimensions)
        firstRow  = int((img.shape[0]-nRows)/2)
        firstCol  = int((img.shape[1]-nCols)/2)
        imgIn     = img[firstRow:firstRow+nRows, firstCol:firstCol+nCols]

        # Feed the LGN cells with the cropped image input (both polarities)
        # LGNBrightInput = LGNBGain*numpy.maximum(0.0, (imgIn.flatten()/127.0-1.0))
        # LGNDarkInput   = LGNDGain*numpy.maximum(0.0, 1.0-(imgIn.flatten()/127.0))        
        LGNBrightInput = LGNBGain*numpy.maximum(0.0, (imgIn.flatten()-greyValue) / (254.0-greyValue))
        LGNDarkInput   = LGNDGain*numpy.maximum(0.0, (greyValue-imgIn.flatten()) / (greyValue - 0.0))
        LGN.amplitude  = numpy.hstack((LGNBrightInput, LGNDarkInput))

