Brain visualizer tutorial
=========================

This tutorial shows you how to use the brain visualizer to highlight what is happenng in your simulated brain.

Your initial move is to connect to the online platform from http://neurorobotics.net if you are a registered user, or to start your locally installed platform.

Then, you need to browse to the template experiments and clone the one called "Tutorial: Brain Visualizer". You can use the filter to find it easily.

Finally, start the simulation by hitting the "Launch" button and follow step 1 hereunder.

.. toctree::

   step_1
   step_2
   step_3
   step_4
