Step 3: fixing the brain file
=============================

Context
^^^^^^^

We have a big brain to process the stimulus, but we still need to feed it with the visual stimulus, and also to record its spikes. Close the brain editor, but let the brain visualizer open. Then open the transfer function editor, to define the spike recorders for all our populations. It's the green button that looks like two plugs. Opening the transfer function editor takes time, because of the size of our brain. Be patient (approx. 1 minute).


Defining new transfer functions to record some spikes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

În the transfer function editor, we already have some transfer functions that you can let as they are. The first thing we want to do is to upload a new transfer function that feeds the robot's brain with the image that is recorded by the camera of the robot. Click on the "upload" button, upload the file number 3 (3_grab_image.py) and hit "add" in the dialog box (never hit "replace").

This transfer function feeds the first layers of the robot's brain (LGN layers) with the output of the camera. Click on apply to take the change into account. Still no spike? That's ok. What we need to do next is to record the spikes of the different layers of the brain. To do so,o click on the "upload" button again to upload the transfer functions numbered from 4a to 4e. Each transfer function records the spikes occuring in a specific layer of the network. Always click on "add" when the dialog box appears.

After you uploaded all the transfer functions, you need to click on "apply all" to take the changes into account. This takes a while to load, because the NRP needs to create a spike recorder for every neuron in the network. But in the end, you start to see some spikes happening in the brain visualizer (don't forget to move the spike contrast cursor)!

However, even if we play with the different display modalities of the brain visualizer (shape and distribution), the spike pattern never really looks like what we would expect. We would like to see the actual stimulus encoded in the brain, just as in the figure of step 2! To do so, we need to customize the brain visualizer. This happens in the next and last step.
